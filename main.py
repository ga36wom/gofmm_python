from tree import *
from rkdt import *
import time
import scipy.io

#### For testing purposes only. ####
def reorderMatrix(K,tree):
    '''
    :param K: Matrix to be reordered
    :param tree: vector containing indeces of the desired reordering
    :return: Matrix that is reordered
    '''
    return K[:,tree][tree]


###### TEST #####

print "## Case 1: Random matrix: Row sampled uniformly ##"

start=time.clock()
### Setup ###
N = 4096  # Use int(sys.argv[1]) to grap from bash , choose multiple of __maxNodes__
leaf_node_size = 32 # Number of nodes in the leaf node
accuracy_skeletons  = 1e-13
max_skeleton_size = 200
k=8 # Number of neighbors
max_level= np.log2(N) - np.log2(leaf_node_size) + 1

print "N=", N, " leaf_size=", leaf_node_size, " accuracy skeletons=", accuracy_skeletons

K = np.random.randn(N,N)

# uncomment for test case

# K = np.load('Matrices/1024.npy')
#K = scipy.io.loadmat('K_4.mat') ['A']
# TODO: change to more meaningful matrix
# A random matrix has very bad HSS behaviour
K = np.random.randn(N,N)

#K = np.genfromtxt('Matrices/K3.dat', delimiter=',')
# Test case 1: type in K manually
#N=4; eps=-2; d=5; K=np.array([[10+eps**2, 0, 2*eps, 2*d],[0, 1+eps**2, 0, 2*eps], [2*eps, 0, 1+eps**2,0],[2*d, 2*eps, 0, 1+eps**2]])
#print K

# Test case 2: Create gram vectors. Plotting optional (only works for 2 dimensions)
#d=100
#phi=np.concatenate((np.concatenate((0.01*np.random.rand(N/2,d)-1e3*np.ones((N/2,d)),np.random.rand(N/2,d)),axis=1),np.concatenate((0.01*np.random.rand(N/2,d)+1e3*np.ones((N/2,d)), np.random.rand(N/2,d)),axis=1)), axis=0)
#plt.plot(phi[:,0],phi[:,1],'rx')
#plt.axis('auto')
#plt.xlabel("Phi")
#plt.ylabel("Phi")
#plt.show()
#print phi
#K=np.dot(phi,np.transpose(phi))

#Test case 3: Check ranks of the matrix
#print np.linalg.matrix_rank(K), 'rank off diag is',np.linalg.matrix_rank(K[N/2:N,0:N/2])
#np.save('unordered',K)

### Build static reference reordering tree ###
head=TreeNode(np.arange(N),None, None)
head.build_tree_COM(K, leaf_node_size, N, N)
tree=head.tree_array()

#Test case 3: check ranks after reordering
#print tree
#print K[:,tree][tree]
#print 'Reordered', np.linalg.matrix_rank(K[:,tree][tree]), 'rank off diag is',np.linalg.matrix_rank(K[:,tree[0:N/2]][tree[N/2:N]])
#np.save('off-diag',K[:,tree[0:N/2]][tree[N/2:N]])
#np.save('reordered', K[:,tree][tree])

### Construct Nearest Neigbors,get candidate lists from randomized trees ###
candidates= ann_using_rkdt(K, N, max_level, k, 10)
neighbors=knn(K, candidates, N, k)


### Build tree node neighbor lists ###
head.build_neighbors(neighbors, leaf_node_size * 2)

setuptime=time.clock()-start
print "Setup-Time", setuptime, "s"
### Skeletonize ###
head.unprunable=True
head.right.unprunable=True
head.right.left.unprunable=True; head.right.right.unprunable=True
head.left.unprunable=True
head.left.left.unprunable=True;head.left.right.unprunable=True
head.skeletonize(N, K, 'neighbors', max_skeleton_size, accuracy_skeletons, False)  # at full matrix there is nothing to skeletonize,

print "## First run: Row sampled from neighbors ##"

### Evaluation ###
for i in range(1,int(max_level)):
    print 'Level', i, ' maximum skeleton approximation s=', max(head.s_for_level(i))
charge=np.ones((N,1))
#charge=np.random.randn(N,1)
head.calc_outgoing(K, charge) # calculate outgiong representations
#head.construct_FMM(charge,K,N)
error=np.zeros(1024)
for i in range (1024):
    randomTestPoints=np.random.randint(N) #evaluate at random points
    #print "Askit evaluated:" , head.evaluate(randomTestPoints,charge,K,N) ,"MatVec" , np.dot(K[randomTestPoints,:],charge)
    error[i]=(head.evaluate(randomTestPoints,charge,K,N)-(np.dot(K[randomTestPoints,:],charge)))/(np.dot(K[randomTestPoints,:],charge))
    #print "relative error", error[i]
    #print "FMM evaluated",  head.evaluate_FMM(randomTestPoints,charge,K,N) ,"MatVec" , np.dot(K[randomTestPoints,:],charge)
print "MEAN Error neighbors: ", np.mean(abs(error))
print "MAX Error neighbors: ", np.max(abs(error))

neighborstime=time.clock()-setuptime
print "Neighbor skeleton+evaluation time", neighborstime , "s"
#head.construct_FMM2(K,N)
#head.evaluate_FMM2(2)
print "## Second run: Rows added uniformly ##"
head.collect_skeletons()
head.set_P_to_None()
head.double_pass(N, K, max_skeleton_size, accuracy_skeletons, False)
head.calc_outgoing(K, charge) # calculate outgiong representations
#head.construct_FMM(charge,K,N)
error=np.zeros(1024)
for i in range (1024):
    randomTestPoints=np.random.randint(N) #evaluate at random points
    #print "Askit evaluated:" , head.evaluate(randomTestPoints,charge,K,N) ,"MatVec" , np.dot(K[randomTestPoints,:],charge)
    error[i]=(head.evaluate(randomTestPoints,charge,K,N)-(np.dot(K[randomTestPoints,:],charge)))/(np.dot(K[randomTestPoints,:],charge))
    #print "relative error", error[i]
    #print "FMM evaluated",  head.evaluate_FMM(randomTestPoints,charge,K,N) ,"MatVec" , np.dot(K[randomTestPoints,:],charge)
for i in range(1,int(max_level)):
    print 'Level', i, ' maximum skeleton approximation s=', max(head.s_for_level(i))
print "MEAN Error: ", np.mean(abs(error))
print "MAX Error: ", np.max(abs(error))
# print "## Second run: Rows sampled from SKELETONS ##"
# head.collect_skeletons()
# head.unprunable=True # at full matrix there is nothing to skeletonize,
# head.set_P_to_None()
# head.skeletonize(N, K, 'skeletons', max_skeleton_size, accuracy_skeletons, False)
# for i in range(1,int(max_level)):
#     print 'Level', i, ' maximum skeleton approximation s=', max(head.s_for_level(i))
# head.set_w_to_None()
# head.calc_outgoing(K, charge)
# for i in range (100):
#     randomTestPoints=np.random.randint(N) #evaluate at random points
#     #print "Askit evaluated:" , head.evaluate(randomTestPoints,charge,K,N) ,"MatVec" , np.dot(K[randomTestPoints,:],charge)
#     error[i]=(head.evaluate(randomTestPoints,charge,K,N)-(np.dot(K[randomTestPoints,:],charge)))/(np.dot(K[randomTestPoints,:],charge))
#     #print "relative error", error[i]
# print "MatVec MEAN Error skeletons: ", np.mean(abs(error))
# print "MatVec MAX Error skeletons: ", np.max(abs(error))
#
# skeletonstime=time.clock()-neighborstime -setuptime
# print "Time for skelton skeletonization+evaluation ",skeletonstime , "s"

