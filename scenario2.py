from tree import *
from rkdt import *
from generate_matrices import *

import time
import scipy.io

#############################################
# This script computes the matvec K*charge  #
# using a gofmm compression and evaluation  #
# scheme                                    #
#############################################


print "## Case 2: Kernel matrix: Row sampled uniformly ##"

start=time.clock()
### Setup ###
N = 4096  # Use int(sys.argv[1]) to grap from bash , choose multiple of __maxNodes__
leaf_node_size = 32 # Number of nodes in the leaf node
accuracy_skeletons  = 1e-13
max_skeleton_size = 200
k=8 # Number of neighbors
max_level= np.log2(N) - np.log2(leaf_node_size) + 1

print "N=", N, " leaf_size=", leaf_node_size, " accuracy skeletons=", accuracy_skeletons


d = 5
h = 0.5
X = np.random.rand(N,d)

K = gaussian_kernel_matrix(X,X,h)

charge=np.ones((N,1))
#charge=np.random.randn(N,1)

### Build static reference reordering tree ###
head=TreeNode(np.arange(N),None, None)
head.build_tree_COM(K, leaf_node_size, N, N)
tree=head.tree_array()

### Construct Nearest Neigbors,get candidate lists from randomized trees ###
# candidates= ann_using_rkdt(K, N, max_level, k, 10)
# neighbors=knn(K, candidates, N, k)

### Build tree node neighbor lists ###
# head.build_neighbors(neighbors, leaf_node_size * 2)


setuptime=time.clock()-start
print "Setup-Time", setuptime, "s"

### Skeletonize ###
head.unprunable=True
head.right.unprunable=True
head.right.left.unprunable=True; head.right.right.unprunable=True
head.left.unprunable=True
head.left.left.unprunable=True;head.left.right.unprunable=True
head.skeletonize(N, K, 'uniform', max_skeleton_size, accuracy_skeletons, False)  # at full matrix there is nothing to skeletonize,

### Evaluation ###
# Only for analysis purposes: I prints (maximum) compression ranks
for i in range(1,int(max_level)):
    print 'Level', i, ' maximum skeleton approximation s=', max(head.s_for_level(i))

# Calculate outgoing representations. In other words, for each charge computes aggregate charge at skeletons
head.calc_outgoing(K, charge) # calculate outgiong representations

# This is (sampled) evaluation and error checking to an exact matvec
error=np.zeros(1024)
for i in range (1024):
    randomTestPoints=np.random.randint(N) #evaluate at random points
    exact = np.dot(K[randomTestPoints,:],charge)
    error[i] = (head.evaluate(randomTestPoints,charge,K,N)-exact)/(exact)
print "MEAN Error neighbors: ", np.mean(abs(error))
print "MAX Error neighbors: ", np.max(abs(error))


print "###################"

