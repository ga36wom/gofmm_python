Installation and Usage
=======
For software dependencies we only rely on `numpy` and `scipy`. 
If you do not have these packages installed already, e.g.:  
* pip install numpy 
* pip install scipy 

Set testing conditions in `main.py` or `scenarioX`. Then execute: 

* python scenarioX.py


You may use the software in the full pipeline (see `main.py`) or for selected functions scripting style only. `main.py` should help in getting started.

## File structure 

main.py -- sets the test case and runs the development algorithms. scenarios are easier

scenario1.py -- random matrix

scenario2.py -- kernel matrix generated from points. - geometry oblivious treatment

scenario3.py -- kernel matrix generated from points. - geometry-clustering and geometry neighbors

scenario4.py -- Stores a matrix and then reads it. Can be adapted to read different formats, binary, .mat, .npy

rkdt.py -- Nearest neighbor algorithms using rkdt; array as tree data structure

tree.py -- Implements tree class and its algorithms

store.py -- script to create and store test matrices K 

generate\_matrices.py -- Kernel matrix creation

## Usage

When wanting to use gofmm for own matvecs, scenario\* should be examples on how to integrate it in your code.
It consists of 5 basic steps:
1. Specify matrix and parameters. *Optional: Specify points. If Matrix originates from point cloud, it usually pays off employing this geometry information.*
2. Setup: Partition tree and compute neighbors for sampling (*using randomized trees*). 
3. Compress: Compute skeletons (i.e. head.skeletonize())
4. Evaluate: Evaluate MatVec at points using compressed scheme
5. (*Optional, but included in examples*) Check accuracy against sampled MatVec 

## `GOFMM` in a nutshell
Given any matrix we find a hierarchical compression.

We look at given matrix **K** as a Gram matrix, i.e. entries can be generated using a scalar product of unknown Gram vectors (**PHI**), **K<sub>ij</sub> =\<PHI<sub>i</sub>,PHI<sub>ij</sub>\>**.

First we permute by clustering the (*unknown*) Gram vector set (*if points given possibility of geometrical clustering*). 
By this, we also build a tree where entries i, j are in the same leaf if ||PHI<sub>i</sub> - PHI<sub>j</sub>|| = K<sub>ii</sub>+K<sub>jj</sub>-2K<sub>ij</sub> is small (*inspired by geometrical clustering*).

Then we compute a *hierarchical compression*:  On far-away blocks we perform a low-rank decomposition (*nested interpolative decomposition*) using nbeighbors as samples (this reqires we computed neighbors before). 

Using the hierarchical compression evaluation schemes are computed in the following way: if none of my neighbors(let's say I am Node A) belong to the corresponding node, I use the compressed version. 
If some of my neighbors are within those that the other tree-node (B) owns, then set the node (B) unprunable and traverse down in the tree. If it's a leaf (B), then compute the matvec between A and B direct (we sometimes refer to it as direct evaluation). 

We show (*approximate*) correctness by checking errors to a sampled matvec, marked at the end of each scenario. 
As sampling size throughout scenarios we use 1024 , but can be adapted.

## Test matrices

In order to reproduce results you may also use the MATLAB script `spdmatrices.m` which stores matrix files binary (currently in single precision floating point format). 

You can apply the software to any binary matrix files. There is also some support for ASCII csv files.

