import numpy as np
from scipy.spatial.distance import cdist

def gaussian_kernel_matrix(targets, sources, bandwidth):
    pairwise_sq_dists = cdist(targets, sources, 'sqeuclidean')
    return np.exp(-pairwise_sq_dists / (2 * bandwidth**2))

def edge_case_1(N,eps, type="normal"):
    phi=np.zeros((N,N))
    for i in range(N):
        phi[0,i]=1#np.random.rand()*200+1
    for i in range(1,N):
        phi[:,i]=phi[:,i-1]
        phi[i][i]=np.random.randint(1,10)
    phi[0,0]=1
    phi[1,:]*=10
    #for i in range(N/2,N):
    #    phi[0,i]=np.random.randint(1,10)
        #phi[1,i]=np.random.rand()*200

    if (type=="opposite"):
        idx_pointing_opposite=np.random.choice(N,N/2)
#        print idx_pointing_opposite
        phi[0,idx_pointing_opposite]=-phi[0,idx_pointing_opposite]
    print "Phi is \n",phi
    return np.dot(np.transpose(phi),phi)