import matplotlib
execfile('main.py')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

matplotlib.pyplot.loglog(np.linalg.svd(K[N/2:N,0:N/2])[1], label="Initial ordering")
matplotlib.pyplot.loglog(np.linalg.svd(K[:,tree[0:N/2]][tree[N/2:N]])[1],'*',label="Algebraic_{COM}")
geom_ordering = np.genfromtxt('Matrices/George/geom_ordering_d_10.dat', delimiter=',',dtype=int)
matplotlib.pyplot.loglog(np.linalg.svd(K[:,geom_ordering[0:N/2]-1][geom_ordering[N/2:N]-1])[1],label="Geometric")
plt.legend(loc=(-1,-1))
plt.xlabel(r"i")
plt.ylabel(r"Singular Value $\sigma_i$")
plt.title("Kp dim=10")