from scipy import random, linalg, dot, diag, all, allclose
import numpy as np
from generate_matrices import *
import time
from tree import *
#N=512 # multiple of __maxNodes__
#d=16
#__maxNodes__=8;
#__maxLevel__=np.log2(N)-np.log2(__maxNodes__)
#phi=np.concatenate((np.concatenate((np.random.rand(N/2,d)-1e4*np.ones((N/2,d)),np.random.rand(N/2,d)),axis=1),np.concatenate((np.random.rand(N/2,d)-1e3*np.ones((N/2,d)), np.random.rand(N/2,d)),axis=1)), axis=0)
#print phi
#K=np.dot(phi,np.transpose(phi))
#N=2048#int(sys.argv[1]) # multiple of __maxNodes__
#d=1
# uncomment for test case
#start= time.clock()
#x=np.random.rand(N,d) # first arg size, second one dimension
#K= gaussian_kernel_matrix(x,x,0.05) # target and sources eqiv
#calc=time.clock()-start
#print 'calc Time' , calc
#np.save('Matrices/2048',K)
#writeTime=time.clock()-(calc)
#print "Writing time", writeTime
#A=np.load("Matrices/2048.npy")
#readingTime=time.clock()-(writeTime+calc)
#print "reading time is", readingTime
#print "A=",A

##Test edge cases
N=8
K=edge_case_1(N,0.1,"opposite") # you can pass ,"opposite") so that a random subset of N/2 gram vectors point in opposite direction
print "Initial K is \n",K
head=TreeNode(np.arange(N),None, None)
head.build_tree_COM(K, 2, N, N)
tree=head.tree_array()
print "Tree is \n",tree
print "Reordered K is \n",K[:,tree][tree]

#from array import array
#output_file = open('file', 'wb+')
#K_ = array('d', array())
#K_.tofile(output_file)
#output_file.close()
#input_file = open('file', 'r')
#float_array = array('d')
#float_array.fromstring(input_file.read())
#print float_array

# for testing qr and runtimes
#[Q,R,__invert__]=linalg.qr(K, pivoting=True)


