import numpy as np
from scipy import linalg
import sys
class TreeNode(object):
    def __init__(self, idxList,left,right):
        self.idx_list=idxList
        self.left=left
        self.right=right
        self.ancestor=None
        self.neighbors=None
        self.selected_rows=None
        self.selected_columns=None # Pi, rows that are pivoted to be in front
        self.selected_columns2=None
        self.P=None
        self.w=None
        self.u=None # For FMM
        self.unprunable=False
        self.s=None
        self.Permute=None
        self.u_bar=None
        self.relevant_skeletons=None
        self.R=None
        self.inv_idx_mapping=None

    def tree_array(self):
        '''
        Creates tree reordering array from object oriented class data structure
        :return: tree reordering array
        '''
        if (self.left is not None):
            return np.append(self.left.tree_array(),self.right.tree_array())
        else: # at leaf
            return self.idx_list

    def build_neighbors(self, knn, lengthLeaf):
        '''
        This function saves in all leaf and interior nodes corresponding neighbors.
        Used to sample target points from node neighbors
        :param knn: Array of size k*N*2, containing in k*N*[0] indices of neighbors and in k*N*[1] its corresponding distances
        :param lengthLeaf: leaf node size
        :return: None.
        '''
        if (self.right==None): # we're at a leaf
            self.neighbors=-np.ones((lengthLeaf,2))
            for i in self.idx_list: # brute force can (needs to) be  changed according to sisc15askit 2.1
                relevantDistance=np.inf
                for xx in range (np.shape(knn[i,:,:])[0]):
                    x=knn[i,xx,:]
                    if (x[1]<relevantDistance and x[0] not in self.idx_list): # is it closer than the previous neighbors? or in cluster
                        if (x[0] in self.neighbors[:,0]): # is point already in
                            position=np.argwhere(self.neighbors[:,0]==x[0])
                            self.neighbors[position,1]=min(self.neighbors[position,1],x[1])
                        else:
                            # fill the array or remove point that is furthest away
                            if (self.neighbors[-1,-1] == -1): #array not full
                                pos=np.argwhere(self.neighbors[:,0]==-1)[0]
                            else:
                                pos=self.neighbors[:,1].argmax()
                            self.neighbors[pos]=x
                            #update max
                            if (self.neighbors[-1,-1] == -1): #array not full
                                relevantDistance=max(self.neighbors[:,1])
            # make sure there are no -1"s anymore. cut away
            ii=0
            for i in range (lengthLeaf):
                if (self.neighbors[ii,0]==-1):
                    self.neighbors=np.delete(self.neighbors, ii,0)
                    ii-=1 #decrease i because we have deleted the row
                ii+=1
            #print 'leaf', np.shape(self.neighbors), len(self.idxList)
        else:
            self.right.build_neighbors(knn, lengthLeaf) # recursion
            self.left.build_neighbors(knn, lengthLeaf) # recursion
            self.neighbors=self.right.neighbors
            for i in xrange (len(self.left.neighbors)):
                if (self.left.neighbors[i,0] not in self.neighbors[:,0]): # if not in other branch
                    self.neighbors=np.vstack((self.neighbors,self.left.neighbors[i]) )
                else: # index is already in right field
                    pos=np.argwhere(self.neighbors[:,0]==self.left.neighbors[i,0])
                    self.neighbors[pos,1]=min(self.neighbors[pos,1],self.left.neighbors[i,1]) # update to closer distance
            # we now have to remove indices which are inside the idxlist
            for x in (self.idx_list):
                if (x in self.neighbors[:,0]):
                    # if neighbors are inside treeNode's indices, remove
                    pos=np.argwhere(self.neighbors[:,0]==x)
                    self.neighbors=np.delete(self.neighbors, pos, 0)
        self.neighbors=np.sort(self.neighbors)

    def skeletonize(self, N, K, type, smax, rtol, verbose):
        '''
        This function calculates a low rank approximation of this node, and all its children.
        It saves the matrix P and selected rows and columns in the treeNode object
        :param verbose:
        :param N: Size of overall Matrix
        :param K: Matrix to be approximated
        :param type: sampling strategy. valid inputs are "uniform" and "sampled"
        :param smax: How many columns should be selected at max
        :param rtol: tolerance desired steer accuracy
        :return: None.
        '''
        if self.unprunable==True:
            if self.left is not None:
                self.left.skeletonize(N, K, type, smax, rtol, False)
                self.right.skeletonize(N, K, type, smax, rtol, False)
                return
        if (self.right!=None and self.left!=None): # interior node, call first on children
            self.right.skeletonize(N, K, type, smax, rtol, False)
            self.left.skeletonize(N, K, type, smax, rtol, False)
            self.selected_columns=np.union1d(self.left.selected_columns, self.right.selected_columns)
            if (self.left.unprunable or self.right.unprunable):
                if verbose:
                    print 'Setting unprunable'
                self.unprunable=True
                return
        else: # at a leaf
            self.selected_columns=self.idx_list
        # draw selected rows l, uniform in N except if part of idx
        rows=2*int(len(self.selected_columns)) # l=2*q...
        self.selected_rows=-np.ones((rows, 1), dtype=int)
        if (type=="uniform"):
            if verbose:
                print 'sampling rows uniformly..'
            for i in range(rows):
                l=-1
                while (l in self.idx_list or l in self.selected_rows or l<0):
                    l=int(np.random.rand()*N)
                self.selected_rows[i]=l
                #print selectedRows
        elif (type=="neighbors"): #sampling from neighbors
            if verbose:
                print 'sampling rows from neighbors..'
            sampled=np.shape(self.neighbors)[0] # number of rows
            l=0
            for i in range(min(sampled,rows)):
                #while (self.neighbors[l,0] in self.selected_rows): #sampling without replacement
                     #l+=1 #np.random.randint(0,sampled)
                self.selected_rows[i]=self.neighbors[i, 1]
                #print self.selected_rows[i]
                #print self.neighbors[i, 0],self.neighbors[i, 1]
            if verbose:
                print "neighbors ", sampled, " to draw ", rows, "rows", "where indexlists length is", len (self.idx_list)
            if (sampled<rows): # if we don't have enough sampling neighbors
                # draw uniformly to fill the rest up
                for i in range(sampled, rows):
                    l=-1
                    while (l in self.idx_list or l in self.selected_rows or l<0):
                        l=int(np.random.rand()*N)
                    self.selected_rows[i]=l
                #print "draw uniformly"
        elif (type=='skeletons'):
            if verbose:
                print 'sampling rows from skeletons of previous skeletonization'
            sampled=len(self.relevant_skeletons)
            # Sampled is number of skeletons available, rows the number of desired
            for i in range(min(sampled,rows)):
                l=-1
                while (l<0 or self.relevant_skeletons[l] in self.selected_rows): #sampling without replacement
                    l=np.random.randint(0,sampled)
                self.selected_rows[i]=self.relevant_skeletons[l]
            if verbose:
                print "skeletons ", sampled, " to draw ", rows, "rows", "where indexlists length is", len (self.idx_list)
            if (sampled<rows):
                # if we don't have enough sampling neighbors
                # draw from neighbors to fill the rest up
                possible_rows = np.setdiff1d(self.neighbors,  self.relevant_skeletons)
                possible_rows = np.setdiff1d(possible_rows, self.idx_list)
                for i in range(sampled, min(sampled+len(possible_rows), rows)):
                    l=-1
                    while (possible_rows[l] in self.idx_list or possible_rows[l] in self.selected_rows or l<0):
                        l=int(np.random.rand()*len(possible_rows))
                    self.selected_rows[i]=possible_rows[l]
                # if we still don't have enough rows, draw uniformly!
                if (sampled+len(possible_rows) < rows):
                    for i in range (sampled+len(possible_rows), rows):
                        while (l in self.idx_list or l in self.selected_rows or l<0):
                            l=int(np.random.rand()*N)
                        self.selected_rows[i]=l

        else:
            if verbose:
                print 'Select valid row sampling scheme for skeletonization'
            exit()
        #building up my matrix
        rows=len(self.selected_rows) #avoid problems if we not have desired numbers of rows
        G=np.zeros((rows,len(self.selected_columns)))
        for i in range(rows):
            jj=0 # upcounting idx for my new array
            for j in (self.selected_columns):
                G[i,jj]=K[int(self.selected_rows[i]), j]
                jj+=1
        #do pivoted qr on G (subsampled off diag matrix of size 2q*q)
        [Q,R,PI]=linalg.qr(G,pivoting=True)
        #adaptive column selelction
        accuracy=1 # initial value to enter loop
        self.s=0 # initial value to enter loop
        #this loop looks at (scaled) diagonal entries of R and estimates how many columns should be selected
        while(abs(accuracy)>rtol and self.s<smax-1 and self.s<len(self.selected_columns)-1):
            accuracy=R[self.s,self.s]* (len(self.idx_list) / len(self.selected_columns)) ** 0.5 * ((N - len(self.idx_list)) / rows) ** 0.5
            self.s+=1
        if verbose:
            print "s=", self.s, "accuracy", accuracy
        # Level restriction: If we cannot approximate the far field sufficiently, set it unprunable
        if (self.s==smax-1):
            self.unprunable=True
            return
        # inverse mapping
        idx_mapping=dict(enumerate(PI))
        inv_map = {v: k for k, v in idx_mapping.items()}
        inv_idx_mapping=inv_map.values()

        # ### Row adaptivity ###
        # # select l additional rows
        row_adaptivity=False
        if (row_adaptivity==True):
            S=np.linalg.svd(R)[1]
            ss=len(self.selected_columns)
            while (True):
                #print len(self.selected_rows), (sampled+len(self.selected_rows), N-len(self.idx_list))
                self.s=ss
                additional_rows=np.zeros(len(self.selected_columns))
                sampled=self.s#np.shape(self.neighbors)[0]
                if (len(additional_rows)+len(self.selected_rows)>=N-len(self.idx_list)):
                    #print "hello"
                    break;
                for i in range(len(self.selected_columns)):
                    if rows+i >= N-len(self.idx_list)-1:
                        if verbose:
                            print "not enough rows to sample from"
                        additional_rows=additional_rows[:i]
                        break
                    if (rows+i<min(sampled-1, np.shape(self.neighbors)[0]-1)):  # we still have neighbors to sample from
                        l=-1
                        while (l<0 or self.neighbors[l,0] in self.selected_rows  or self.neighbors[l, 0] in additional_rows):
                            l=np.random.randint(0,sampled)
                        additional_rows[i]=self.neighbors[l,0]
                        #print 'additional rows from neighbors..'
                    else: # sample uniformly
                        l=-1
                        #print 'additional rows uniformly..'
                        #print len(self.idx_list)
                        while (l in self.idx_list or l in self.selected_rows or l in additional_rows or l<0):
                            l=int(np.random.rand()*N)
                        additional_rows[i]=l
                        # #print len(self.selectedRows), len(additional_rows)
                        # # allocate K_tilde
                K_tilde=np.zeros((len(self.selected_columns), len(self.selected_columns)))
                for i in range(len(additional_rows)):
                    jj=0 # upcounting idx for my new array
                    for j in (self.selected_columns):
                        K_tilde[i,jj]=K[int(additional_rows[i]),j]
                        jj+=1
                #print "PI",PI ,"\n dict" , idx_mapping, "\n inv", inv_idx_mapping
                #calculate QR of additional targets
                [QQ,RR,PIPI]=linalg.qr(np.vstack((R[:,inv_idx_mapping],K_tilde)), pivoting=True)
                SS=np.linalg.svd(np.vstack((R[:,inv_idx_mapping],K_tilde)))[1]
                #compute s, adapt if too much different
                accuracy2=1
                ss=0
                while(abs(accuracy2)>rtol and ss<min(smax-1, len(self.selected_columns)-1)):
                    ss+=1
                    accuracy2=RR[ss,ss]* (len(self.idx_list) / len(self.selected_columns)) ** 0.5 * ((N - len(self.idx_list)) / (rows+len(additional_rows))) ** 0.5
                if verbose:
                    print "ROW added ss=", ss, "accuracy", accuracy2
                    # #adaptation correction? To be implemented
                print "norm", np.linalg.norm(RR[0:len(self.selected_columns),:])
                print (np.linalg.norm(RR[0:len(self.selected_columns),:])-np.linalg.norm(R[0:len(self.selected_columns),:]))#/np.linalg.norm(RR[0:len(self.selected_columns),:])
                print np.linalg.norm(RR[0:len(self.selected_columns),:]-R[0:len(self.selected_columns),:])/np.linalg.norm(RR[0:len(self.selected_columns),:])
                if np.linalg.norm(R[0:len(self.selected_columns),:]-RR[0:len(self.selected_columns),:])<0.001:
                    #print "no of rows:", len(self.selected_rows), "difference (sigma'[s]-sigma[s])/sigma'[s]",abs((SS[ss]/np.sqrt(len(self.selected_rows)+len(additional_rows))-S[ss]/np.sqrt(len(self.selected_rows)))/(SS[ss]np.sqrt(len(self.selected_rows)+len(additional_rows))))
                    self.s=ss
                    break
                #print "Row adaption was necessary"
                R=RR
                PI=PIPI
                idx_mapping=dict(enumerate(PI))
                inv_map = {v: k for k, v in idx_mapping.items()}
                inv_idx_mapping=inv_map.values()
                self.selected_rows=np.append(self.selected_rows,additional_rows)
                #print S[self.s-1] ,SS[ss], abs((SS[ss]-S[ss])/SS[ss])
                #print self.s, ss
                #self.s=len(self.selected_columns)
            R=RR
            PI=PIPI
            idx_mapping=dict(enumerate(PI))
            inv_map = {v: k for k, v in idx_mapping.items()}
            inv_idx_mapping=inv_map.values()
            self.selected_rows=np.append(self.selected_rows,additional_rows)
        # print "here"

        G_col= G[:,PI[0:self.s]]
        R12= R[0:self.s, self.s:len(self.idx_list)] # in askit papers often referred to as R_{1,2}
        R11=R[0:self.s,0:self.s]
        P_right=linalg.solve(R11,R12)
        self.P=np.hstack((np.eye(self.s),P_right))
        self.P=self.P[:,inv_idx_mapping]
        #repeat row sampling?  To be implemented
        # Naive accuracy checking
        if (verbose and self.left is not None):
            print np.shape(K[np.setdiff1d(np.arange(0,N),self.left.idx_list),:][:, self.selected_columns][:, PI]), 'is approximated by a mult of ' ,np.shape(K[np.setdiff1d(np.arange(0, N), self.left.idx_list), :][:, self.selected_columns[PI[0:self.s]]]), 'paired with P being' ,np.shape(self.P)
            print 'using a QR deco of size', np.shape(G)
            print ' and is accurate?',np.linalg.norm(K[np.setdiff1d(np.arange(0,N),self.left.idx_list),:][:, self.selected_columns][:, PI] - np.dot(K[np.setdiff1d(np.arange(0, N), self.left.idx_list), :][:, self.selected_columns[PI[0:self.s]]], self.P)), "whole rows"
        self.Permute=PI
        self.selected_columns2=self.selected_columns[PI[self.s:]]
        self.selected_columns=self.selected_columns[PI[0:self.s]]


    def double_pass(self, N, K, smax, rtol, verbose):
        '''
        This function enhances a previous skeletonziation of this node, and all its children.
        It saves the matrix P and selected rows and columns in the treeNode object
        :param verbose: prints out some debug lines
        :param N: Size of overall Matrix
        :param K: Matrix to be approximated
        :param type: sampling strategy. valid inputs are "uniform" and "sampled"
        :param smax: How many columns should be selected at max
        :param rtol: tolerance desired steer accuracy
        :return: None.
        '''

        ### Call function post-order traversal on children
        if self.unprunable==True:
            if self.left is not None:
                self.left.double_pass(N, K,  smax, rtol, False)
                self.right.double_pass(N, K, smax, rtol, False)
                return
            return
        if (self.right!=None and self.left!=None): # interior node, call first on children
            self.right.double_pass(N, K, smax, rtol, False)
            self.left.double_pass(N, K,  smax, rtol, False)
            self.selected_columns=np.union1d(self.left.selected_columns, self.right.selected_columns)
            if (self.left.unprunable or self.right.unprunable):
                if verbose:
                    print 'Setting unprunable'
                self.unprunable=True
                return
        else: # at a leaf
            self.selected_columns=self.idx_list
        ### Draw rows from skeletons
        if verbose:
            print 'sampling rows from skeletons of previous skeletonization'
        additional_rows=np.setdiff1d(self.relevant_skeletons, self.selected_rows)
        additional_rows=additional_rows[additional_rows != np.array(None)]
        #print "Length of additional rows", len(additional_rows)
        if (len(additional_rows)==0):
            return
        #allocate G from previous (differs through potentially different set of columns)
        rows=len(self.selected_rows) #avoid problems if we not have desired numbers of rows
        G=np.zeros((rows,len(self.selected_columns)))
        for i in range(rows):
            jj=0 # upcounting idx for my new array
            for j in (self.selected_columns):
                G[i,jj]=K[int(self.selected_rows[i]), j]
                jj+=1
        # allocate K_tilde
        K_tilde=np.zeros((len(additional_rows), len(self.selected_columns)))
        for i in range(len(additional_rows)):
            jj=0
            for j in (self.selected_columns):
                K_tilde[i,jj]=K[int(additional_rows[i]),j]
                jj+=1

        #calculate QR of additional targets
        [QQ,RR,PIPI]=linalg.qr(np.vstack((G,K_tilde)), pivoting=True)
        #compute s, adapt if too much different
        # "We converge when the approximation rank remains constant between two iterations"
        accuracy2=1
        self.s=0
        while(abs(accuracy2)>rtol and self.s<min(smax-1, len(self.selected_columns))):
            accuracy2=RR[self.s,self.s]* (len(self.idx_list) / len(self.selected_columns)) ** 0.5 * ((N - len(self.idx_list)) / (rows+len(additional_rows))) ** 0.5
            self.s+=1
        #print "ROW added ss=", ss, "accuracy", accuracy2, "previous s=", self.s
        #adaptation correction? To be implemented
        #print "Skeleton Row adaption was necessary"
        R=RR
        PI=PIPI
        idx_mapping=dict(enumerate(PI))
        inv_map = {v: k for k, v in idx_mapping.items()}
        inv_idx_mapping=inv_map.values()
        #G_col= G[:,PI[0:self.s]]
        R12= R[0:self.s, self.s:len(self.idx_list)] # in askit papers often referred to as R_{1,2}
        R11=R[0:self.s,0:self.s]
        P_right=linalg.solve(R11,R12)
        self.P=np.hstack((np.eye(self.s),P_right))
        self.P=self.P[:,inv_idx_mapping]
        self.Permute=PI
        self.selected_columns2=self.selected_columns[PI[self.s:]]
        self.selected_columns=self.selected_columns[PI[0:self.s]]
        #repeat row sampling?  To be implemented

    def add_uniform_samples(self,N, K, smax, rtol, verbose):
        '''
        This function enhances a previous skeletonziation of this node and all its children.
        by adding rows uniformly (as comparison to double pass)
        It saves the matrix P and selected rows and columns in the treeNode object
        :param verbose: prints out some debug lines
        :param N: Size of overall Matrix
        :param K: Matrix to be approximated
        :param type: sampling strategy. valid inputs are "uniform" and "sampled"
        :param smax: How many columns should be selected at max
        :param rtol: tolerance desired steer accuracy
        :return: None.
        '''
        ### Call function post-order traversal on children
        if self.unprunable==True:
            if self.left is not None:
                self.left.add_uniform_samples(N, K,  smax, rtol, False)
                self.right.add_uniform_samples(N, K, smax, rtol, False)
                return
            return
        if (self.right!=None and self.left!=None): # interior node, call first on children
            self.right.add_uniform_samples(N, K, smax, rtol, False)
            self.left.add_uniform_samples(N, K,  smax, rtol, False)
            self.selected_columns=np.union1d(self.left.selected_columns, self.right.selected_columns)
            if (self.left.unprunable or self.right.unprunable):
                if verbose:
                    print 'Setting unprunable'
                self.unprunable=True
                return
        else: # at a leaf
            self.selected_columns=self.idx_list
        # sample rows uniformly at random
        no_additional_rows = len(np.setdiff1d(self.relevant_skeletons, self.selected_rows))
        #print "hello", no_additional_rows
        additional_rows=-np.ones((no_additional_rows,1))
        for i in range(no_additional_rows):
            l=-1
            while (l in self.idx_list or l in self.selected_rows or l in additional_rows or l<0):
                l=int(np.random.rand()*N)
            additional_rows[i]=l
            #print selectedRows
                #allocate G from previous (differs through potentially different set of columns)
        rows=len(self.selected_rows) #avoid problems if we not have desired numbers of rows
        G=np.zeros((rows,len(self.selected_columns)))
        for i in range(rows):
            jj=0 # upcounting idx for my new array
            for j in (self.selected_columns):
                G[i,jj]=K[int(self.selected_rows[i]), j]
                jj+=1
        # allocate K_tilde
        K_tilde=np.zeros((len(additional_rows), len(self.selected_columns)))
        for i in range(len(additional_rows)):
            jj=0
            for j in (self.selected_columns):
                K_tilde[i,jj]=K[int(additional_rows[i]),j]
                jj+=1

        #calculate QR of additional targets
        [QQ,RR,PIPI]=linalg.qr(np.vstack((G,K_tilde)), pivoting=True)
        #compute s, adapt if too much different
        # "We converge when the approximation rank remains constant between two iterations"
        accuracy2=1
        self.s=0
        while(abs(accuracy2)>rtol and self.s<min(smax-1, len(self.selected_columns))):
            accuracy2=RR[self.s,self.s]* (len(self.idx_list) / len(self.selected_columns)) ** 0.5 * ((N - len(self.idx_list)) / (rows+len(additional_rows))) ** 0.5
            self.s+=1
        #print "ROW added ss=", ss, "accuracy", accuracy2, "previous s=", self.s
        #adaptation correction? To be implemented
        #print "Skeleton Row adaption was necessary"
        R=RR
        PI=PIPI
        idx_mapping=dict(enumerate(PI))
        inv_map = {v: k for k, v in idx_mapping.items()}
        inv_idx_mapping=inv_map.values()
        #G_col= G[:,PI[0:self.s]]
        R12= R[0:self.s, self.s:len(self.idx_list)] # in askit papers often referred to as R_{1,2}
        R11=R[0:self.s,0:self.s]
        P_right=linalg.solve(R11,R12)
        self.P=np.hstack((np.eye(self.s),P_right))
        self.P=self.P[:,inv_idx_mapping]
        self.Permute=PI
        self.selected_columns2=self.selected_columns[PI[self.s:]]
        self.selected_columns=self.selected_columns[PI[0:self.s]]


    def build_tree_geometric(self,dist_matrix,maxLeafsize, n, N):
        '''
        Builds a static tree using a distance matrix
        :param dist_matrix:
        :param maxLeafsize:
        :param n: local node size
        :param N: global node size
        :return:
        '''
        #allocate a distance projection matrix containing proj value nu and index
        temp=np.zeros((n,1),dtype=[('nu',float),('index',int)])
        approx=int(np.log(n)) #number of summation entries for approximation
        test_points=np.random.choice(self.idx_list, size=approx, replace=False)
        ii=0
        # Compute distance to approximate center: Note, in this computation I leave out sum(K_ii), to have real numeric
        # 4distances one would have to add those. however, only scaling
        for i in self.idx_list:
            temp[ii]=(0,i)
            temp['nu'][ii]+=sum(dist_matrix[i, test_points])
            ii+=1
        ptFurthestAwayFromCenter= np.argmax(temp['nu'])
        idxPtFurthestAwayFromCenter=self.idx_list[ptFurthestAwayFromCenter]
        # distance to this point
        ii=0
        for i in self.idx_list:
            temp[ii]=(dist_matrix[i,idxPtFurthestAwayFromCenter],i)
            ii+=1
        otherEnd=np.argmax(temp['nu'])
        idxOtherEnd=self.idx_list[otherEnd]
        ii=0
        for i in self.idx_list:
            temp[ii]=(dist_matrix[i,idxPtFurthestAwayFromCenter]-dist_matrix[i,idxOtherEnd],i)
            ii+=1
        temp= np.sort(temp,axis=0)
        # Recursive call
        if (n/2>=maxLeafsize):
            self.left=TreeNode(np.transpose(temp['index'][0:n/2])[0],None, None)
            self.left.build_tree_geometric(dist_matrix, maxLeafsize, n / 2, N)
            self.left.ancestor=self
            self.right=TreeNode(np.transpose(temp['index'][n/2:n])[0],None, None)
            self.right.build_tree_geometric(dist_matrix, maxLeafsize, n / 2, N)
            self.right.ancestor=self



    def build_tree_COM (self, K, maxLeafsize, n, N):
        '''

        This function builds a static tree, splitting for points farthest away from (approx.) center of mass and its adversary
        :param K: Matrix to be build up
        :param maxLeafsize: leaf node size
        :param n: size of current treeNode
        :param N: size of K
        :return: None
        '''
        #allocate a distance projection matrix containing proj value nu and index
        temp=np.zeros((n,1),dtype=[('nu',float),('index',int)])
        approx=int(np.log(n)) #number of summation entries for approximation
        ii=0
        #Compute distance to approximate center: Note, in this computation I leave out sum(K_ii), to have real numeric distances one would have to add those. however, only scaling
        for i in self.idx_list:
            temp[ii]=(K[i,i],i)
            temp['nu'][ii]-=2*sum(((K[i,(np.random.choice(self.idx_list, size=approx, replace=False))])))/N
            ii+=1
        #Select point farthest away from center
        ptFurthestAwayFromCenter= np.argmax(temp['nu'])
        idxPtFurthestAwayFromCenter=self.idx_list[ptFurthestAwayFromCenter]
        #calculate distances of each point to this point
        # ||Phi_j-Phi_i||=K_jj+K_ii-2K_ij, K__jj again only scaling, left out
        ii=0
        for i in self.idx_list:
            temp[ii]=((K[i,i])-2*(K[i,idxPtFurthestAwayFromCenter]),i)
            ii+=1
        otherEnd=np.argmax(temp['nu'])
        idxOtherEnd=self.idx_list[otherEnd]
        #Project points onto ptFurthestAwayFromCenter and otherEnd
        ii=0
        #print idxPtFurthestAwayFromCenter, idxOtherEnd
        for i in self.idx_list:
            temp[ii]=((((K[i,idxOtherEnd]))-(K[i,idxPtFurthestAwayFromCenter])),i)
            #print temp[ii]
            ii+=1
        #split at median
        temp= np.sort(temp,axis=0)
        # Recursive call
        if (n/2>=maxLeafsize):
            self.left=TreeNode(np.transpose(temp['index'][0:n/2])[0],None, None)
            self.left.build_tree_COM(K, maxLeafsize, n / 2, N)
            self.left.ancestor=self
            self.right=TreeNode(np.transpose(temp['index'][n/2:n])[0],None, None)
            self.right.build_tree_COM(K, maxLeafsize, n / 2, N)
            self.right.ancestor=self
        #print(self.idxList)

    def calc_outgoing(self, K, w):
        '''
        This function calculates the outgoing representation depending on a charge vector
        Saves w tilde in object
        :param K: Matrix to be approximated
        :param w: charge vector
        :return: None
        '''
        # post order traversal, top down
        if (self.left is not None): # interior node, we assume balnaced tree
            self.left.calc_outgoing(K, w)
            self.right.calc_outgoing(K, w)
            if (self.P is not None):# it's (theoretically prunable)
                #print len(self.idx_list )
                self.w=np.dot(self.P,np.vstack((self.left.w,self.right.w))) # do I need to be careful with indices? No, because columns coresponding to left and right child are chosen
        else:
            if (self.P is not None): # it's (theoretically prunable)
                self.w=np.dot(self.P, w[self.idx_list])

    def evaluate(self, desiredPoint, charge, K, N):
        '''
        Performs the evaluation steps. Pruns everything to far field, if Node not unprunable
        :param desiredPoint: index of desired matrix evaluation
        :param charge: vector of charges (size N*1)
        :param K: Matrix to be approximated
        :param N: size of Matrix
        :return: Result of Node's evaluation
        '''
        res=0
        if (self.left is not None and self.right is not None): # interior node
            if (self.unprunable):
                res+=self.left.evaluate(desiredPoint,charge,K,N)
                res+=self.right.evaluate(desiredPoint,charge,K,N)
                return res
            elif (desiredPoint in self.left.idx_list): # we don't prone the left node
                res+=np.dot(K[desiredPoint,self.right.selected_columns],self.right.w)
                res+=self.left.evaluate(desiredPoint,charge,K,N)
                return res
            else:
                res+=np.dot(K[desiredPoint,self.left.selected_columns],self.left.w)
                res+=self.right.evaluate(desiredPoint,charge,K,N)
                return res
        else: # at leaf
            return np.dot(K[desiredPoint,self.idx_list], charge[self.idx_list])

    def construct_FMM(self, charge, K, N):
        '''
        Calculates skeleton charges for the FMM.
        :param desiredPoint: index of desired matrix evaluation
        :param charge: vector of charges (size N*1)
        :param K: Matrix to be approximated
        :param N: size of Matrix
        :return: Result of Node's evaluation
        '''
        res=0
        if (self.left is not None and self.right is not None): # interior node
            self.left.construct_FMM(charge,K,N)
            self.right.construct_FMM(charge,K,N)
            return
        else: # at leaf
            # We calculate an incoming representation for all leaf nodes
            print np.shape(np.dot(K[self.selected_columns,:][:,self.selected_columns],self.w))
            self.u = np.dot(K[self.selected_columns,:][:,self.selected_columns],self.w)[:,0]
            print np.shape(self.u)
            self.u += self.outgoing_incoming(charge,K,N, self.selected_columns)

    def outgoing_incoming(self, charge,K,N, target_points):
        res=np.zeros(len(target_points))
        if (self.ancestor.ancestor is not None):
            res+=self.ancestor.outgoing_incoming(charge,K,N,target_points)
            print np.shape(res)
            if (self is self.ancestor.left):
                print np.shape(res)
                res+=self.ancestor.right.evaluate_outgoing_incoming(charge,K,N,target_points)
            else:
                res+=self.ancestor.left.evaluate_outgoing_incoming(charge,K,N,target_points)
        return res

    def evaluate_outgoing_incoming(self, charge, K, N,target_points):
        res=np.zeros(len(target_points))
        if (self.unprunable and self.right is not None):
            res=self.right.evaluate_outgoing_incoming(charge,K,N,target_points)
            res+=self.left.evaluate_outgoing_incoming(charge,K,N,target_points)
            print np.shape(res)
            return res
        else:
            res=np.dot(K[target_points,:][:,self.selected_columns],self.w)
            return res[0]

    def evaluate_FMM(self, desiredPoint, charge, K, N):
        '''
        Performs the evaluation steps. Pruns everything to far field, if Node not unprunable
        :param desiredPoint: index of desired matrix evaluation
        :param charge: vector of charges (size N*1)
        :param K: Matrix to be approximated
        :param N: size of Matrix
        :return: Result of Node's evaluation
        '''
        res=0
        if (self.left is not None and self.right is not None): # interior node
            if (desiredPoint in self.left.idx_list):
                return self.left.evaluate_FMM(desiredPoint,charge,K,N)
            elif (desiredPoint in self.right.idx_list):
                return self.right.evaluate_FMM(desiredPoint,charge,K,N)
            else:
                print "desired point not in tree"
        else:
            #print self.u
            return np.dot(np.transpose(self.P),self.u)[np.argwhere(self.idx_list==desiredPoint)]

    def construct_FMM2(self, K,N):
        if (self.unprunable==False):
            if (self == self.ancestor.left):
                if (self.u_bar is None):
                    self.u_bar=np.zeros(self.s)
                print np.shape(self.u_bar)
                print "G", np.shape(K[self.selected_columns,:][:,self.ancestor.right.selected_columns])
                self.u_bar += np.dot(K[self.selected_columns,:][:,self.ancestor.right.selected_columns],self.ancestor.right.w)[0]
                print np.shape(self.u_bar)
            else:
                if (self.u_bar is None):
                    self.u_bar=np.zeros(self.s)
                self.u_bar += np.dot(K[self.selected_columns,:][:,self.ancestor.left.selected_columns],self.ancestor.left.w)[0]
            print len(self.selected_columns), "other" , len(self.selected_columns2), len(self.Permute)
            if (self.left is not None):
                self.left.u_bar=np.zeros(len(self.left.selected_columns))
                self.right.u_bar=np.zeros(len(self.right.selected_columns))
                #now permute step necessary
                print np.hstack((self.selected_columns,self.selected_columns2))
                print min(self.selected_columns),max(self.selected_columns)
                print np.shape(self.u_bar)
                print np.shape(self.P)
                wlwr=np.dot(np.transpose(self.P),self.u_bar)
                print np.shape(wlwr)
                print "incoming", np.shape(wlwr[self.Permute[:len(self.left.Permute)]])
                self.left.u_bar=wlwr[self.Permute[:len(self.left.Permute)]]
                self.right.u_bar=wlwr[self.Permute[len(self.left.Permute):]]
        if (self.left is not None):
            self.left.construct_FMM2(K,N)
            self.right.construct_FMM2(K,N)
    def evaluate_FMM2(self,desiredPoint):
        if (self.left is not None):
            if desiredPoint in self.left.idx_list:
                self.left.evaluate_FMM2(desiredPoint)
            elif desiredPoint in self.right.idx_list:
                self.right.evaluate_FMM2(desiredPoint)
            else:
                print "Desired Point not available"
        else:
            print self.unprunable, "u_bar=", np.shape(self.u_bar)
            print np.dot(self.P[:,desiredPoint],self.u_bar)

    def collect_skeletons(self):
        if self.ancestor is not None:
            # get the skeleton from the sibling
            if self.relevant_skeletons is None:
                self.relevant_skeletons = set()
            if self is self.ancestor.left:
                if self.ancestor.right.selected_columns is not None:
                    for x in self.ancestor.right.selected_columns:
                        self.relevant_skeletons.add(x)
            else:
                if self.ancestor.left.selected_columns is not None:
                    for x in self.ancestor.left.selected_columns:
                        self.relevant_skeletons.add(x)
        if self.left is not None and self.right is not None:
            if self.relevant_skeletons is not None:
                self.left.relevant_skeletons=set(self.relevant_skeletons)
                self.right.relevant_skeletons=set(self.relevant_skeletons)
            else:
                self.left.relevant_skeletons=set()
                self.right.relevant_skeletons=set()
            # remove skeletons that are inside leaf (idxlist)
            self.left.relevant_skeletons.discard(x for x in self.left.idx_list)
            self.right.relevant_skeletons.discard(x for x in self.right.idx_list)
            self.left.collect_skeletons()
            self.right.collect_skeletons()
        if self.relevant_skeletons is not None:
            #print "relevant skeleton size=" , len(self.relevant_skeletons), " where idxList is long ", len(self.idx_list)
            # Have skeletons as array
            self.relevant_skeletons=np.array(list(self.relevant_skeletons))

    def set_w_to_None(self):
        self.w=None
        if self.left is not None:
            self.left.set_w_to_None()
            self.right.set_w_to_None()
    def set_P_to_None(self):
        self.P=None
        self.Permute=None
        self.selected_columns=None
        if self.left is not None:
            self.left.set_P_to_None()
            self.right.set_P_to_None()

    def s_for_level(self,level):
        a = list()
        if (level==0 and self is not None):
            if self.s is not None:
                a.append(self.s)
                return a
            else:
                a.append("Unprunable")
                return a
        elif (level>0 and self is not None):
            a.extend(self.left.s_for_level(level-1))
            a.extend(self.right.s_for_level(level-1))
            return a
        else:
            print "provided level not suitable"
            return




#obsolete, used to create a object oriented tree structure from reordering tree
def arrayToBST(array,maxNodeSize):
    left=None
    right=None
    if (array.__len__()>maxNodeSize):
        left=arrayToBST(array[0:int((array.__len__())/2)],maxNodeSize)
        right=arrayToBST(array[int((array.__len__())/2):array.__len__()],maxNodeSize)
    return treeNode(array,left,right)

